module com.mycompany.clasejavafx {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.mycompany.clasejavafx to javafx.fxml;
    exports com.mycompany.clasejavafx;
}
