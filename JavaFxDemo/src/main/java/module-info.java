module com.mycompany.javafxdemo {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.mycompany.javafxdemo to javafx.fxml;
    exports com.mycompany.javafxdemo;
}
