/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conversionfx.Controller;

import com.mycompany.conversionfx.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Sistemas-12
 */
public class MainFXmlController implements Initializable {

    /**
     *
     */
    @FXML
    public Button btnTemperatura;
    
    /**
     *
     */
    @FXML
    public Button btnCurrency;
    
    @FXML
    public BorderPane pnlBorder;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    
    @FXML
    public void btnTemperaturaAction() throws IOException{
        Node parent =App.loadFXML("Temperaturafxml");
        
    }
    
    
    @FXML
    public void btnCurrencyAction(){
        
    }
   
}
