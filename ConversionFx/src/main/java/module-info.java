module com.mycompany.conversionfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;

    opens com.mycompany.conversionfx to javafx.fxml;
    exports com.mycompany.conversionfx;
    exports com.mycompany.conversionfx.Controller;
    
}
